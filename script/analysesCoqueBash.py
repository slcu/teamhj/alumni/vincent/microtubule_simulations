#!/usr/bin/python
# -*- coding: utf-8 -*- 
#
# author : Vincent Mirabet
#
#   Cette version considère que la coque de la cellule n'est pas dans le résultat
#
import numpy as np
from tvtk.api import tvtk
import os, sys
import scipy
import scipy.spatial.distance
import cPickle
#from mayavi import mlab

 
class CoqueErrorException(Exception):
    pass

class NumberOfArgumentsException(Exception):
    pass

class FileAlreadyAnalysed(Exception):
    pass

def subSpatial(d, size):
    """
    assign cube subspace to each element based on position
    """
    red=lambda X:np.round(X/size)
    d3=d[:,0:3].copy()
    spatial=red(d3[:,0:3])
    d2=np.hstack((d,spatial))
    return d2, d

def neighbourZones(d, (ii,jj,kk)):
    '''
    searches for neighbouring cubes
    '''
    condGen=np.zeros(len(d)).astype(np.bool)
    for i in [-1,0,1]:
        for j in [-1,0,1]:
            for k in [-1,0,1]:
                condGen+= ( d[:,8]==ii + i) *  (d[:,9]==jj + j) * (d[:,10]==kk + k)
    condRest=( d[:,8]==ii ) *  (d[:,9]==jj) * (d[:,10]==kk)
    return condRest, condGen

def lecture(nom, rep="./"):
    r=tvtk.PolyDataReader()
    r.file_name=rep+nom
    r.update()
    p=r.output
    return p

def lectureCoque(nom):
    r=tvtk.PolyDataReader()
    r.file_name=nom
    r.update()
    p=r.output
    return p

def ajoutCoque(p,d,verbose=False):
    """
    cette fonction n'est utile que dans le cas où le contour de la cellule n'était pas incorporé à la simulation
    """
    #à priori le type est 2 et l'identité est 1 dans la situation 'à coque'
    if 2 in np.unique(d[:,7]):
        raise CoqueErrorException("il y a une coque dans les données")
    
    x=np.array([X[0] for X in p.points])
    y=np.array([X[1] for X in p.points])
    z=np.array([X[2] for X in p.points])
    vx=np.array([X[0] for X in p.point_data.get_array(0)])
    vy=np.array([X[1] for X in p.point_data.get_array(0)])
    vz=np.array([X[2] for X in p.point_data.get_array(0)])
    
    identite=np.array([1 for i in p.point_data.get_array(0)])
    if verbose:print p.point_data.get_array_name(1)
    typ=np.array([2 for i in p.point_data.get_array(0)])
    
    dCoque=np.array((x,y,z,vx,vy,vz,identite,typ)).T
    d2=np.concatenate((dCoque,d))
    return d2

def filtreBundle(d,verbose=True):
    """
    TODO il faudrait enlever le verbose=True quand c'est fini
    cette fonction a pour but de générer un d sans bundles
    """
    toto={}
    tata={}
    for r in d:
        toto.setdefault(tuple(np.round(r[3:6],3)),[]).append(r)
        tata.setdefault(tuple(-np.round(r[3:6],3)),[]).append(r)

    #on met dans toto toutes les directions uniques de microtubules. Certaines sont ainsi associées à plusieurs lignes de la matrice.
    clefs=tata.keys()
    l=len(toto.keys())
    for i,k in enumerate(toto.keys()):
        try:
            toto[k].extend(tata[k])
            #print k, i/float(l), len(toto[k])
            print i/float(l)
        except Exception as e:
            #print "erreur", e
            pass
        l=[]
    for i in toto:
        l.append(np.mean(toto[i],axis=0))

    df=np.array(l)
    return df
    


def extraction(p,verbose=0):
    """
    0, 1, 2, 3 , 4,  5,  6,        7
    X, Y, Z, VX, VY, VZ, identite, type
    
    c'est ici qu'il faut que je rajoute la coque si elle n'y est pas spontanément.
    """
    x=np.array([X[0] for X in p.points])
    y=np.array([X[1] for X in p.points])
    z=np.array([X[2] for X in p.points])
    vx=np.array([X[0] for X in p.point_data.get_array(0)])
    vy=np.array([X[1] for X in p.point_data.get_array(0)])
    vz=np.array([X[2] for X in p.point_data.get_array(0)])
    identite=np.array([i for i in p.point_data.get_array(1)])
    if verbose:print p.point_data.get_array_name(1)
    typ=np.array([i for i in p.point_data.get_array(2)])
    d=np.array((x,y,z,vx,vy,vz,identite,typ)).T
    return d

def calculate_distances(d):
    """
    vérifier les histoires de cond, condr poru distance1/distance2
    
    pour le moment cette fonction ne marche pas...
    dist2[d[:,7]==2]
    """
    directions={}
    distance1=np.ones(d.shape[0])*-1
    distance2=np.ones(d.shape[0])*-1
    nb=0.
    longueur=len(list(set([tuple(i) for i in d[:,8:11]])))
    pour=0
    for ii,jj,kk in list(set([tuple(i) for i in d[:,8:11]])):
        if int(nb/longueur*10)*10==pour:
            pass
        else:
            print int(nb/longueur*10)*10,"%","(",ii,jj,kk,")"
            pour=int(nb/longueur*10)*10
        nb+=1
        #take the subspace and its neighbours
        condr, cond=neighbourZones(d, (ii,jj,kk))
        dsub=d[cond]
        dsubr=d[condr]
        dsubn=d[cond - condr]
        #########subspace :
        #coordinates of tubulines
        Xr=dsubr[:,0:3][dsubr[:,7]==1]    
        #directions of tubulins of the subpsace
        VXr=dsubr[:,3:6][dsubr[:,7]==1]
        #coordinates of the mb
        Xm=dsub[:,0:3][dsub[:,7]==2]
        #directions of the mb normals
        Xmn=dsub[:,3:6][dsub[:,7]==2]
        ###########subpsace and its neighbours
        #coordinates of tubulines
        X=dsub[:,0:3][dsub[:,7]==1]
        #directions of tubulins
        VX=dsub[:,3:6][dsub[:,7]==1]
        ###########only neighbours : souci si vide
        #coordinates of tubulines
        #Xn=dsubn[:,0:3][dsubn[:,7]==1]
        #directions of tubulins
        #VXn=dsubn[:,3:6][dsubn[:,7]==1]
        #calculations
        axi,bxi=np.ix_(Xr[:,0],Xm[:,0])
        ayi,byi=np.ix_(Xr[:,1],Xm[:,1])
        azi,bzi=np.ix_(Xr[:,2],Xm[:,2])
        Xri,Xi=np.ix_(Xr[:,0],X[:,0])
        Yri,Yi=np.ix_(Xr[:,1],X[:,1])
        Zri,Zi=np.ix_(Xr[:,2],X[:,2])
        Ident=dsub[:,6][dsub[:,7]==1]
        Identr=dsubr[:,6][dsubr[:,7]==1]
        #################################distances between points
        matrice_distance1=scipy.spatial.distance.cdist(Xr,X)
        #matrice points - membrane (sans correction de plan)
        #matrice_distance1=(axi-bxi)*(axi-bxi) + (ayi-byi)*(ayi-byi) + (azi-bzi)*(azi-bzi)
        distances_1_dsubr=np.ones(len(dsubr))*-1
        #print len(Identr), matrice_distance1.shape[0] ok
        for k in range(matrice_distance1.shape[0]):
            ref=Identr[k]
            if (len(matrice_distance1[k])>0) and len(matrice_distance1[k][Ident!=ref])>0:
                #must not be the same tubulin, must not be on the same microtubule
                cand=matrice_distance1[k] == np.min(matrice_distance1[k][Ident!=ref])
                dist=np.abs(float(matrice_distance1[k,cand][0]))
                #print dist, ref, Ident
                distances_1_dsubr[k]=dist
        distance1[condr]=distances_1_dsubr
        #################################distances between points and mb
        #matrice scalaires : point-mb * normale
        matrice_distance2=np.abs((axi-bxi)*Xmn[:,0] + (ayi-byi)*Xmn[:,1] + (azi-bzi)*Xmn[:,2])
        distances_2_dsubr=np.ones(len(dsubr))*-1
        for k in range(matrice_distance2.shape[0]):
            if len(matrice_distance2[k])>0:
                ref=Identr[k]
                #print ref
                cand=matrice_distance2[k] == np.min(matrice_distance2[k])
                dist=np.abs(float(matrice_distance2[k,cand][0]))
                distances_2_dsubr[k]=dist
        distance2[condr]=distances_2_dsubr
        #################################directions (FAUT IL NORMALISER?)
        #directions[(ii,jj,kk)]=np.outer(np.mean(VXr, axis=0), np.mean(VXn, axis=0))
    d1={}
    d2={}
    for k,i in enumerate(d):
        d1.setdefault((i[8],i[9],i[10]),[]).append(distance1[k])
        d2.setdefault((i[8],i[9],i[10]),[]).append(distance2[k])
    for i in d1:
        d1[i]=np.array(d1[i])
    for i in d2:
        d2[i]=np.array(d2[i])
    return d1, d2, distance1, distance2


def save(d, nom):
    np.savetxt("toto.txt.gz",d)


def taille(vec):
    return np.sqrt((np.sum(vec**2)))

def normalise(vec):
    res= vec/taille(vec)
    return res


def vv(toto):
    corr=1./toto.shape[0]*np.dot(toto.T, toto)
    e=np.linalg.eig(corr)
    m=e[1]
    ee=e[0]
    return ee, [m[0,:], m[1,:], m[2,:]]
    #return ee, [m[:,0], m[:,1], m[:,2]]

def plotMt(d, opacite=0.1):
    x=d[:,0][d[:,7]==1]
    y=d[:,1][d[:,7]==1]
    z=d[:,2][d[:,7]==1]
    vx=d[:,3][d[:,7]==1]
    vy=d[:,4][d[:,7]==1]
    vz=d[:,5][d[:,7]==1]
    mlab.quiver3d(x,y,z,vx,vy,vz, color=(0,1,0), scale_factor=20, opacity=opacite)
    #mlab.points3d(x,y,z, d[:,10][d[:,7]==1], scale_factor=0.1)
    
def plotMoy(points_space,vectors_space):
    x,y,z,vx,vy,vz=[],[],[],[],[],[]
    for i in points_space:
        for k,l in enumerate(vectors_space[i]):
            x.append(points_space[i][0])
            y.append(points_space[i][1])
            z.append(points_space[i][2])
            vx.append(vectors_space[i][:,0][k])
            vy.append(vectors_space[i][:,1][k])
            vz.append(vectors_space[i][:,2][k])
    mlab.quiver3d(x,y,z,vx,vy,vz, color=(1,1,1), scale_factor=20, opacity=0.1)
    #mlab.points3d(x,y,z, d[:,10][d[:,7]==1], scale_factor=0.1)


def plotPd(points_space, vv_space, princ_space, opacite=1):
    x,y,z,vx,vy,vz=[],[],[],[],[],[]
    vx1,vy1,vz1=[],[],[]
    vx2,vy2,vz2=[],[],[]
    scal=[]
    for i in points_space:
        x.append(points_space[i][0])
        y.append(points_space[i][1])
        z.append(points_space[i][2])
        x.append(points_space[i][0])
        y.append(points_space[i][1])
        z.append(points_space[i][2])
        x.append(points_space[i][0])
        y.append(points_space[i][1])
        z.append(points_space[i][2])
        vx.append(vv_space[i][1][0][0])
        vy.append(vv_space[i][1][1][0])
        vz.append(vv_space[i][1][2][0])
        vx.append(vv_space[i][1][0][1])
        vy.append(vv_space[i][1][1][1])
        vz.append(vv_space[i][1][2][1])
        vx.append(vv_space[i][1][0][2])
        vy.append(vv_space[i][1][1][2])
        vz.append(vv_space[i][1][2][2])
        scal.append(vv_space[i][0][0])
        scal.append(vv_space[i][0][1])
        scal.append(vv_space[i][0][2])
    x=np.array(x)
    y=np.array(y)
    z=np.array(z)
    vx=np.array(vx)
    vy=np.array(vy)
    vz=np.array(vz)
    mlab.quiver3d(x,y,z,vx,vy,vz, color=(1,0,0),scalars=scal, scale_mode='scalar', scale_factor=100, opacity=opacite)
    mlab.quiver3d(x,y,z,-vx,-vy,-vz, color=(1,0,0),scalars=scal, scale_mode='scalar', scale_factor=100, opacity=opacite)
        
def test_calcule_microtubules_anisotropie(ecrire=0):
    p=lecture("test.vtk")
    d=extraction(p)
    tens,v,p=calcule_microtubule_tenseurs(d, True)
    tens_c,v_c,p_c=calcule_contour_tenseurs(d,True)
    if ecrire:
        if ecrire == 1:
            print tens
        if ecrire == 2:
            print tens,v,p
    for k in tens.keys()[0:1]:
        ee,m = tens[k]
        x,y,z=p[k][:,0],p[k][:,1],p[k][:,2]
        x2,y2,z2=np.array([np.mean(x) for i in range(3)]),np.array([np.mean(y) for i in range(3)]),np.array([np.mean(z) for i in range(3)])
        vx,vy,vz=v[k][:,0],v[k][:,1],v[k][:,2]
        print k, len(x), len(vx), m
        mlab.quiver3d(x,y,z,vx, vy, vz, color=(0,0,1))
        mlab.quiver3d(x2,y2,z2, m[0], m[1], m[2], color=(0,0,1),scalars=ee, scale_mode='scalar',scale_factor=100, line_width=5)
        mlab.quiver3d(x2,y2,z2, -m[0], -m[1], -m[2], color=(0,0,1),scalars=ee, scale_mode='scalar',scale_factor=100, line_width=5)
    for k in tens_c.keys()[0:1]:
        ee,m = tens_c[k]
        x,y,z=p_c[k][:,0],p_c[k][:,1],p_c[k][:,2]
        x2,y2,z2=np.array([np.mean(x) for i in range(3)]),np.array([np.mean(y) for i in range(3)]),np.array([np.mean(z) for i in range(3)])
        vx,vy,vz=v_c[k][:,0],v_c[k][:,1],v_c[k][:,2]
        print k, len(x), len(vx), m
        mlab.quiver3d(x,y,z,vx, vy, vz, color=(0,0,1))
        mlab.quiver3d(x2,y2,z2, m[0], m[1], m[2], color=(0,0,1),scalars=ee, scale_mode='scalar',scale_factor=100, line_width=5)
        mlab.quiver3d(x2,y2,z2, -m[0], -m[1], -m[2], color=(0,0,1),scalars=ee, scale_mode='scalar',scale_factor=100, line_width=5)


def plot_tens(tens,p,k):
    xa,ya,za=p[k][:,0],p[k][:,1],p[k][:,2]
    x2,y2,z2=np.mean(p[k][:,0]),np.mean(p[k][:,1]),np.mean(p[k][:,2])
    ee,m = tens[k]
    x,y,z=[0,0,0],[0,0,0],[0,0,0]
    x[0],y[0],z[0]=m[0][0], m[1][0], m[2][0]
    x[1],y[1],z[1]=m[0][1], m[1][1], m[2][1]
    x[2],y[2],z[2]=m[0][2], m[1][2], m[2][2]
    print x,y,z,x2,y2,z2,ee
    mlab.points3d(xa,ya,za, color=(0,0,1),scale_factor=1)
    k1,k2,k3=np.argsort(ee)
    print k1,k2,k3
    mlab.quiver3d(x2,y2,z2,x[k1], y[k1], z[k1], color=(1,0,0),scalars=ee[k1], scale_mode='scalar',scale_factor=100, line_width=5)
    mlab.quiver3d(x2,y2,z2,x[k2], y[k2], z[k2], color=(0,1,0),scalars=ee[k2], scale_mode='scalar',scale_factor=100, line_width=5)
    mlab.quiver3d(x2,y2,z2,x[k3], y[k3], z[k3], color=(0,0,1),scalars=ee[k3], scale_mode='scalar',scale_factor=100, line_width=5)

def test_quantification_calcule_microtubules_anisotropie(ecrire=0):
    p=lecture("test.vtk")
    d=extraction(p)
    tens,v,p=calcule_microtubule_tenseurs(d, True)
    tens_c,v_c,p_c=calcule_contour_tenseurs(d,True)
    if ecrire:
        if ecrire == 1:
            print tens
        if ecrire == 2:
            print tens,v,p
    plot_tens(tens_c,p_c,1)
    for k in tens.keys()[0:1]:
        plot_tens(tens,p,k)
    

        



def test():
    t=1000.
    toto=np.random.random((t,3))-1/2.
    toto=toto*np.array([1,5,1])
    toto=np.array([normalise(vecteur) for vecteur in toto])
    x,y,z=np.random.random(t)*0,np.random.random(t)*0,np.random.random(t)*0
    ee,m=vv(toto)
    x2,y2,z2=np.array([np.mean(x) for i in range(3)]),np.array([np.mean(y) for i in range(3)]),np.array([np.mean(z) for i in range(3)])
    mlab.quiver3d(x2,y2,z2,m[0], m[1], m[2], color=(0,0,1),scalars=ee, scale_mode='scalar', line_width=5)
    mlab.quiver3d(x2,y2,z2,-m[0], -m[1], -m[2], color=(0,0,1),scalars=ee, scale_mode='scalar', line_width=5)
    mlab.quiver3d(x,y,z,toto[:,0], toto[:,1], toto[:,2], opacity=0.1)

def calcule_microtubule_tenseurs(d, tout=False):
    vects={}
    pos={}
    tens={}
    for m in d:
        if m[7]!=2:
            vects.setdefault(m[6],[]).append(m[3:6])
            pos.setdefault(m[6],[]).append(m[0:3])
    for k in vects.keys():
        vects[k]=np.array(vects[k])
        pos[k]=np.array(pos[k])
        tens[k]=vv(vects[k])
    if tout:
        return tens, vects, pos
    else:
        return tens


def calcule_contour_tenseurs(d, tout=False):
    vects={}
    pos={}
    tens={}
    for m in d:
        if m[7]==2:
            vects.setdefault(m[6],[]).append(m[3:6])
            pos.setdefault(m[6],[]).append(m[0:3])
    for k in vects.keys():
        vects[k]=np.array(vects[k])
        pos[k]=np.array(pos[k])
        tens[k]=vv(vects[k])
    if tout:
        return tens, vects, pos
    else:
        return tens

def calcule_discretisation(d):
    """
    d est le gros tableau général
    """
    result=d.copy()
    spaces=sorted(list(set([tuple(i) for i in result[:,8:11]])))
    vectors_space={}
    points_space={}
    princ_space={}
    for i in spaces:
        res=result[np.where((result[:,8]==i[0]) * (result[:,9]==i[1]) * (result[:,10]==i[2]) * (result[:,7]==1))]
        for j in res:
            #print j[0:3], j[3:6], taille(j[3:6])
            vectors_space.setdefault(i,[]).append(j[3:6])
            points_space.setdefault(i,[]).append(j[0:3])
    for i in vectors_space:
        vectors_space[i]=np.array(vectors_space[i])
        points_space[i]=np.mean(np.array(points_space[i]), axis=0)
    vv_space={}
    for i in spaces:
        res=result[np.where((result[:,8]==i[0]) * (result[:,9]==i[1]) * (result[:,10]==i[2]) * (result[:,7]==1))]
        for j in res:
            vv_space[i]=vv(vectors_space[i])
            princ_space[i]=np.argsort(vv_space[i][0])
    return points_space,vectors_space, vv_space, princ_space 

def calcul_anisotropie(vv_space, princ_space):
    ani={}
    trois={}
    for i in sorted(vv_space.keys()):
        vals=vv_space[i][0][princ_space[i][::-1]]
        ani[i]=vals[1]/vals[0]
        trois[i]=vals
    return ani, trois


def donnees_distance(d1,d2):
    """sub contient :
        - moyenne, std des valeurs non -1
        - le nombre de valeurs, le nombre de -1
        - pour les deux distances

    """
    
    sub={}
    for i in d1:
        nb1=len(d1[i])
        nb_out1=len(d1[i][d1[i]==-1])
        if nb1==nb_out1:
            moy1=-3
            std1=-3
        else:
            moy1=np.mean(d1[i][d1[i]!=-1])
            std1=np.std(d1[i][d1[i]!=-1])
        nb2=len(d2[i])
        nb_out2=len(d2[i][d2[i]==-1])
        if nb2==nb_out2:
            moy2=-3
            std2=-3
        else:
            moy2=np.mean(d2[i][d2[i]!=-1])
            std2=np.std(d2[i][d2[i]!=-1])
        toto=[moy1, std1, nb1, nb_out1, moy2, std2, nb2, nb_out2]
        sub[i]=toto
    return sub


def donnees_anisotropie(dori):
    """
            sub contient :
            - les trois valeurs d'anisotropie
    """
    points_space,vectors_space, vv_space, princ_space = calcule_discretisation(dori)
    ani, vals=calcul_anisotropie(vv_space, princ_space)
    sub={}
    for i in ani:
        sub[i]=vals[i]
    return sub

def donnees_generales(d, dist1, dist2):
    """
    donne :
    - nb de tubulines
    - nb de microtubules
    - longueur moyenne des microtubules
    - écart type de cette longueur
    - longueurs
    - distance pt-mb moyenne
    - distance pt-mb std
    - distance pt-pt moyenne
    - distance pt-pt std
    - nb de distances < seuil
    """
    pass
    nb_tubulines=len(d[d[:,7]==1])
    nb_microtubules=len(np.unique(d[:,6]))-1
    longueur=[]
    for i in np.unique(d[:,6]):
        if i != 1:
            longueur.append(len(d[d[:,6]==i]))
    longueur=np.array(longueur)
    lg_moy=np.mean(longueur)
    lg_std=np.std(longueur)
    d_ptpt_moy=np.mean(dist1[dist1>=0])
    d_ptpt_std=np.std(dist1[dist1>=0])
    d_ptmb_moy=np.mean(dist2[dist2>=0])
    d_ptmb_std=np.std(dist2[dist2>=0])
    nb_bundle=len(dist1[(dist1>=0)*(dist1<=6.125)])
    return nb_tubulines, nb_microtubules, longueur, lg_moy, lg_std, d_ptpt_moy, d_ptpt_std, d_ptmb_moy, d_ptmb_std, nb_bundle


def write_gen(rep,nom3,d, dist1, dist2,pre):
    nb_tubulines, nb_microtubules, longueur, lg_moy, lg_std, d_ptpt_moy, d_ptpt_std, d_ptmb_moy, d_ptmb_std, nb_bundle=donnees_generales(d, dist1, dist2)
    f3=open(rep+nom3, "a")
    ligne=[nb_tubulines, nb_microtubules,longueur, lg_moy, lg_std, d_ptpt_moy, d_ptpt_std, d_ptmb_moy, d_ptmb_std, nb_bundle]
    f3.write(";".join([str(j) for j in pre]))
    f3.write(";")
    f3.write(";".join([str(val) for val in ligne]))
    f3.write("\n")
    f3.close()

def write_ani(d,rep,nom2,pre):
    #SPATIALISATION
    # donnees issues de la spatialisation en blocs à 50
    d, dsave=subSpatial(d,50)
    ani50=donnees_anisotropie(d)
    # creation de la spatialisation en blocs à 100
    d, dsave=subSpatial(dsave,100)
    ani100=donnees_anisotropie(d)
    # creation de la spatialisation en blocs à 200
    d, dsave=subSpatial(dsave,200)
    ani200=donnees_anisotropie(d)
    # creation de la spatialisation en blocs à 300
    d, dsave=subSpatial(dsave,300)
    ani300=donnees_anisotropie(d)
    # creation de la spatialisation en blocs à 400
    d, dsave=subSpatial(dsave,400)
    ani400=donnees_anisotropie(d)
    #print result
    f2=open(rep+nom2, "a")
    for i in ani50:
        f2.write(";".join([str(j) for j in pre]))
        f2.write(";50;")
        f2.write(";".join([str(np.real(j)) for j in ani50[i]]))
        f2.write("\n")
    for i in ani100:
        f2.write(";".join([str(j) for j in pre]))
        f2.write(";100;")
        f2.write(";".join([str(np.real(j)) for j in ani100[i]]))
        f2.write("\n")
    for i in ani200:
        f2.write(";".join([str(j) for j in pre]))
        f2.write(";200;")
        f2.write(";".join([str(np.real(j)) for j in ani200[i]]))
        f2.write("\n")
    for i in ani300:
        f2.write(";".join([str(j) for j in pre]))
        f2.write(";300;")
        f2.write(";".join([str(np.real(j)) for j in ani300[i]]))
        f2.write("\n")
    for i in ani400:
        f2.write(";".join([str(j) for j in pre]))
        f2.write(";400;")
        f2.write(";".join([str(np.real(j)) for j in ani400[i]]))
        f2.write("\n")
    f2.close()

def main():
    """
    TODO : changer ici pour rendre compatible avec une version ne lisant qu'un fichier
    """
    args = sys.argv[1:]
    if len(args)!=4:
        print "give the folder, the name of the file, the path/name of the contour, verbose"
        raise NumberOfArgumentsException("Thou shall give 4 parameters!")
    rep=args[0]
    nom=args[1]
    verbose=int(args[3])
    if nom[-4:]!=".vtk":
        if verbose:print "mauvais fichier", nom[-4:]
        sys.exit(0)
    coque=args[2]
    #ici un flag dans le répertoire
    p=lecture(nom, rep)
    p2=lectureCoque(coque)
    pre=nom[:-4].split("_")
    #ici on récupère les données en matrices
    if verbose:print "départ"
    dprovi=extraction(p)
    if verbose:print "extraction"
    d=ajoutCoque(p2,dprovi)
    if verbose:print "ajoutCoque"
    #DONNEES GENERALES
    df=filtreBundle(dprovi)
    if verbose:print "filtreBundle"
    d, dsave=subSpatial(d,400)
    if verbose:print "subspatial"
    d1, d2, dist1, dist2=calculate_distances(d)
    if verbose:print "calculate_distances"
    #write_dir()
    try:
        write_gen(rep,nom[:-4]+"-gen.txt",d, dist1, dist2,nom[:-4].split("_"))
    except :
        print "error in gen writing"
    try:
        write_ani(df,rep,nom[:-4]+"-ani.txt",nom[:-4].split("_"))
    except :
        print "error in ani writing"
    if verbose:print "write"


if __name__ == '__main__':
    main()








