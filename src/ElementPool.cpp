 //elementPool
#include "ElementPool.hpp"
#include "Element.hpp"
#include "Shape.hpp"
#include <vector>
#include <algorithm>
#include "math.h"

#define verbose 0
#define deb(x) if (verbose == 1){cout << x << endl;}
#define deb2(x) if (verbose == 2){cout << x << endl;}

using namespace std;


    std::ostream& operator<<(std::ostream& os, ElementPool& p)
    {
        return os;
    }
    
    ElementPool::ElementPool(Parametres *params)
    {
        if (verbose >= 1) {cout<<"constructeur d'elementPool :"<<endl;}
        m_maxId=-1;
        //TODO ici lier tout cela a la taille de la structure
//         int maxX = 1000;
//         int maxY = 1000;
//         int maxZ = 1000;
        m_params=params;
        m_hashContour_resolution=25;
        m_hashMt_resolution=25;
        int taille = 256;
        m_spaceContour2id = new std::vector<Element *>[taille*taille*taille];
        m_spaceMt2id = new std::vector<Element *>[taille*taille*taille];
        
        rencontre_bundle_grow = 0;//au voisin
        rencontre_bundle_shrink = 0;//au voisin
        rencontre_mb_grow = 0;//à la membrane
        rencontre_mb_shrink = 0;//à la membrane
        cross = 0;//situation à la membrane où l'on crosse
        quitte_mb=0;
        scission = 0;//événement de scission
    }
    
    ElementPool::~ElementPool()
    {       
           if (verbose >=1 ) {cout<<"destructeur d'elementPool :"<<endl;}
           for (vector<Element*>::iterator it = m_elements.begin(); it!= m_elements.end();it++)
           {
               delete *it;
           }
           delete [] m_spaceContour2id;
           delete [] m_spaceMt2id;

    }
    
    Element* ElementPool::giveElement()
    {
        deb("giveElement() _1");
        Id current=m_maxId+1;
        deb("giveElement() _2");
        m_maxId++;
        Element* e = new Element(m_params, this, current);
        deb("giveElement() _3");
        m_elements.push_back(e);
        deb("giveElement() _4");
        //m_vivants.push_back(e);
        deb("giveElement() _5");
        return e;
    }
    
    Element* ElementPool::giveElement(Structure* id)
    {
        deb("giveElement(Id id, int type)");
        Id current=m_maxId+1;
        m_maxId++;
        Element* e = new Element(m_params, this, current);
        e->setStructure(id);
        m_elements.push_back(e);
        //m_vivants.push_back(e);
        return e;
    }

//TODO  Ici doubler cette fonction : pour l'espace du contour et l'espace des microtubules
    void ElementPool::spaceContourRegister(Element* e)
    {
        deb("ElementPool::spaceContourRegister(Element* e)");
		long pos;
        deb("ElementPool::spaceContourRegister | this->hashContour(e)");
		pos = this->hashContour(e);
		deb("ElementPool::spaceContourRegister | m_id2spaceContour[e]=pos;");
        m_id2spaceContour[e]=pos;
        deb("ElementPool::spaceContourRegister | m_spaceContour2id[pos].push_back(e)");
        //printf("%d\n", pos);
		m_spaceContour2id[pos].push_back(e);
	}
    
    void ElementPool::spaceMtRegister(Element* e)
    {
        deb("ElementPool::spaceMtRegister(Element* e");
		long pos;
		pos = this->hashMt(e);
		m_id2spaceMt[e]=pos;
		m_spaceMt2id[pos].push_back(e);
	}
    

    
//TODO celle la est sans doute useless dans un contexte de pointeur
    //Element* ElementPool::getElement(Id id, string s)
    //{
        //ici ajouter des tests de coherence
        //deb("entree dans getElement");
        //if (id >= m_elements.size() )
		//{
			//cout<< "key does not exist in elementPool, from "<<s << endl;
			//exit(EXIT_FAILURE);
		//}
        //return m_elements[id];
    //}



//TODO celle ci devra prendre en compte les deux espaces
//deux versions possibles : on supprime bien l'élément ou on le tue juste.
// ici on le tue
    //void ElementPool::erase(Element *e)
    //{
		//std::vector<Element*>* vec=&m_spaceContour2id[m_id2spaceContour[e]];
		//vec->erase(remove(vec->begin(), vec->end(), e), vec->end());
        //vec=&m_spaceMt2id[m_id2spaceMt[e]];
		//vec->erase(remove(vec->begin(), vec->end(), e), vec->end());
        //
        //e->setDead();
        //m_vivants.erase(remove(vec->begin(), vec->end(), e), vec->end());
        //
		//m_id2spaceContour.erase(e);
        //m_id2spaceMt.erase(e);
	//}
    //
 //ici on le supprime
void ElementPool::erase(Element *e)
{
deb("entree dans erase(Element *e)");
        e->setPrevious(NULL);
        e->setDead();
        m_deads.push_back(e);
        m_nbdeads+=1;
        unlist_tocut(e);
        //delete e;
deb("sortie de erase(Element *e)");    
	}
    
    //

void ElementPool::garbageCollector()
{
deb2("garbage");
    m_elements.erase(
    std::remove_if(m_elements.begin(),m_elements.end(), [](Element *e) { return !(e->isAlive()); }),
    m_elements.end());

    for (vector<Element*>::iterator it=m_deads.begin();it!=m_deads.end();it++)
    {
        std::vector<Element*>* vec=&m_spaceContour2id[m_id2spaceContour[*it]];
		vec->erase(remove(vec->begin(), vec->end(), *it), vec->end());
        vec=&m_spaceMt2id[m_id2spaceMt[*it]];
		vec->erase(remove(vec->begin(), vec->end(), *it), vec->end());
        //m_vivants.erase(remove(vec->begin(), vec->end(), e), vec->end());
		m_id2spaceContour.erase(*it);
        m_id2spaceMt.erase(*it);
        unlist_tocut(*it);
        delete *it;
    }
    
    resetDead();
}


void ElementPool::enlist_tocut(Element *e)
{
    if (!(e->getPropriete("to_cut")))
    {
        e->setPropriete("to_cut",1);
        m_tocut.push_back(e);
    }
}

void ElementPool::unlist_tocut(Element *e)
{
    m_tocut.remove(e);
    e->setPropriete("to_cut",0);
}

std::list<Element *> ElementPool::getlist_tocut()
{
    return(m_tocut);
}



void ElementPool::resetDead()
{
    m_nbdeads=0;
    m_deads.clear();
}
    
//TODO cette fonction est obsolète si toutes les listes sont des listes de pointeurs
    list<Element *> ElementPool::getListElements()
    {
        list<Element *> lm;
        for (vector<Element*>::iterator it=m_elements.begin();it!=m_elements.end();it++)
        {
            lm.push_back(*it);
        }
        return lm;
    }

//TODO cette fonction est sans doute obsolète dans un contexte de pointeurs
    list<Element *> ElementPool::getListId2Space()
    {
		list<Element *> lm;
        for (unordered_map<Element *,long>::iterator it=m_id2spaceMt.begin();it!=m_id2spaceMt.end();it++)
        {
            lm.push_back(it->first);
        }
        return lm;
	}


    int ElementPool::maxId()
    {
        return m_maxId;
    }


//gestion de la partie espace
std::vector<Element *>* ElementPool::getSpaceContour()
{
    return m_spaceContour2id;
}

std::vector<Element *>* ElementPool::getSpaceMt()
{
    return m_spaceMt2id;
}



std::vector<Element *> ElementPool::getSpaceContourContent(double x, double y, double z)
{
	vector<Element *> liste;
	int verif=0;
	for (int i=-1; i<2; i++)
	{
		for (int j=-1; j<2; j++)
		{
			for (int k=-1; k<2; k++)
			{
				verif++;
				double x2 = x+i*m_hashContour_resolution;
				double y2 = y+j*m_hashContour_resolution;
				double z2 = z+k*m_hashContour_resolution;
				if ((x2>=0) & (y2>=0) & (z2>=0) )
				{
                    long h = hashContour(x2,y2,z2);
                    std::vector<Element *>* m_spaceContour2id_p = &(m_spaceContour2id[h]);
                    liste.insert(liste.end(), m_spaceContour2id_p->begin(), m_spaceContour2id_p->end());
				}
			}
		}
	}
	//cout<<liste.size()<<" " <<m_space2id[h].size() <<endl;
    return liste;
}


vector<Element *> ElementPool::getSpaceMtContent(double x, double y, double z)
{
	vector<Element *> liste;
	int verif=0;
	for (int i=-1; i<2; i++)
	{
		for (int j=-1; j<2; j++)
		{
			for (int k=-1; k<2; k++)
			{
				verif++;
				double x2 = x+i;
				double y2 = y+j;
				double z2 = z+k;                
				if ((x2>=0) & (y2>=0) & (z2>=0) )
				{
                    long h = hashMt(x2,y2,z2);
                    std::vector<Element *>* m_spaceMt2id_p = &(m_spaceMt2id[h]);
                    liste.insert(liste.end(), m_spaceMt2id_p->begin(), m_spaceMt2id_p->end());
				}
			}
		}
	}
	//cout<<liste.size()<<" " <<m_space2id[h].size() <<endl;
    return liste;
}





//HASH CONTOUR
long ElementPool::hashContour(Element* id) 
{
    long h;
    h=hashContour(*(id->getAnchor()));
    return h;
}

long ElementPool::hashContour(Anchor a)
{
    int x = *a.getX() / m_hashContour_resolution;
    int y = *a.getY() / m_hashContour_resolution;
    int z = *a.getZ() / m_hashContour_resolution;
    long c;
    x <<= 16;
	y <<= 8;
    c=x|y|z;
    return c;
}

long ElementPool::hashContour(double x, double y, double z)
{
    int x2 = x / m_hashContour_resolution;
    int y2 = y / m_hashContour_resolution;
    int z2 = z / m_hashContour_resolution;
    long c;
    x2 <<= 16;
	y2 <<= 8;
    c=x2|y2|z2;
    return c;
}


long ElementPool::getPositionContour(Element* id)
{
	return m_id2spaceContour[id];
}




//HASH MT
long ElementPool::hashMt(Element* id) 
{
    long h;
    h=hashMt(*(id->getAnchor()));
    return h;
}

long ElementPool::hashMt(Anchor a)
{
    int x = *a.getX() / m_hashMt_resolution;
    int y = *a.getY() / m_hashMt_resolution;
    int z = *a.getZ() / m_hashMt_resolution;
    long c;
    x <<= 16;
	y <<= 8;
    c=x|y|z;
    return c;
}

long ElementPool::hashMt(double x, double y, double z)
{
    int x2 = x / m_hashMt_resolution;
    int y2 = y / m_hashMt_resolution;
    int z2 = z / m_hashMt_resolution;
    long c;
    x2 <<= 16;
	y2 <<= 8;
    c=x2|y2|z2;
    return c;
}


long ElementPool::getPositionMt(Element* id)
{
	return m_id2spaceMt[id];
}


void ElementPool::setContour(Contour *c)
{
    m_contour=c;
}

Contour * ElementPool::getContour()
{
    return m_contour;
}

int ElementPool::testRecursif(Element *candidat, Element *e, int degre)
{
    Element *p = e->getPrevious();
    for (int i = 0; i<degre; i++)
    {
        if (p != NULL)
        {
            if (p != candidat)
            {
                p=p->getPrevious();
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 1;
        }
    }
    return 1;

}






//TODO : reflechir a classer les elements en amont lors de leur stockage pour éviter les calculs inutiles
pair<double, Element*> ElementPool::NN_Mt(Element* plus)
{
    deb("NN_Mt");
// 	long h;
//     Structure * microtubule_id = plus->getStructure();
// 	h=hashMt(*(plus->getAnchor()));
	vector<double> pos_plus;
	pos_plus.push_back( *plus->getAnchor()->getX() );
	pos_plus.push_back( *plus->getAnchor()->getY() );
	pos_plus.push_back( *plus->getAnchor()->getZ() );
	deb("NN_Mt 1");
	pair<double, Element*> min1;
	min1.first=-1;
	min1.second=NULL;  
	std::vector<Element*> content = getSpaceMtContent( pos_plus[0], pos_plus[1], pos_plus[2] );
	int k=0;
	if (content.empty())
	{
        deb("NN_Mt 2 if");
		Shape s(0.,0.,0.);
		min1.first=-1.;
		min1.second=NULL;
		return min1;
	}
	else
	{
        deb("NN_Mt 2 else");
		min1.first=100000.;
		for (vector<Element*>::iterator it=content.begin(); it!=content.end(); it++)
		{
			Element* e = *it;
            if ( ( e != plus ) && ( e->isAlive() ) )
            {
                double* x=e->getAnchor()->getX();
                double* y=e->getAnchor()->getY();
                double* z=e->getAnchor()->getZ();
                double d = pow(pos_plus[0] - *x, 2) + pow(pos_plus[1] - *y, 2) + pow(pos_plus[2] - *z, 2) ;
                //this line should solve the detection of itself in the calculation, BEWARE : if the size of o
                //interaction is not adequate a microtubule could auto-influence its own direction
                if (  ( testRecursif(e, plus, 10) ) )
                {   //ici le souci c'est que le suivant est à 2... je vais patcher en calculant si les précédents sont encore présents
                    k++;
                    if (d < min1.first)
                    {
                        min1.first=d;
                        min1.second=(*it);
                    }
                }
            }
		}
        min1.first=sqrt(min1.first);
		//cout << "nb de tests NN : " << k <<" "<< content.size() << endl;
        deb("NN_Mt fin");
		return min1;
	}
}


//TODO : reflechir a classer les elements en amont lors de leur stockage pour éviter les calculs inutiles
pair<double, Element*> ElementPool::NN_Contour(Element* plus)
{
    deb("NN_Contour");
// 	long h;
// 	Structure * microtubule_id = plus->getStructure();
// 	h=hashContour(*(plus->getAnchor()));
	vector<double> pos_plus;
	pos_plus.push_back( *plus->getAnchor()->getX() );
	pos_plus.push_back( *plus->getAnchor()->getY() );
	pos_plus.push_back( *plus->getAnchor()->getZ() );
	
	pair<double, Element*> min1;
	min1.first=-1;
	min1.second=NULL;  
	std::vector<Element*> content = getSpaceContourContent( pos_plus[0], pos_plus[1], pos_plus[2] );
	int k=0;
	if (content.empty())
	{
		Shape s(0.,0.,0.);
		min1.first=-1.;
		min1.second=NULL;
		return min1;
	}
	else
	{
		min1.first=100000.;
		for (vector<Element*>::iterator it=content.begin(); it!=content.end(); it++)
		{
			Element* e = *it;
            if ( ( e != plus ) && ( e->isAlive() ) )
            {
                double* x=e->getAnchor()->getX();
                double* y=e->getAnchor()->getY();
                double* z=e->getAnchor()->getZ();
                double d = pow(pos_plus[0] - *x, 2) + pow(pos_plus[1] - *y, 2) + pow(pos_plus[2] - *z, 2) ;
                k++;
                if (d < min1.first)
                {
                    min1.first=d;
                    min1.second=(*it);
                }
            }
		}
        min1.first=sqrt(min1.first);
		//cout << "nb de tests NN : " << k <<" "<< content.size() << endl;
        deb("NN_Contour fin");
		return min1;
	}
}


















