//shape of an element
#ifndef __SHAPE_H_INCLUDED__
#define __SHAPE_H_INCLUDED__
#include <vector>
class Shape
{
    public:
        Shape();
        Shape(double x, double y, double z);

        std::vector<double> getDirection() const;
        void setDirection(std::vector<double> direction);
    
    private:
    //par defaut au moins un vecteur direction synthetique
        std::vector<double> m_direction;

};
#endif
