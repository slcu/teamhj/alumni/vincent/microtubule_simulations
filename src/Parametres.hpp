//classe PARAMETRES
//non si parametres.h deja importe ailleurs
#ifndef __PARAMETRES_H_INCLUDED__
#define __PARAMETRES_H_INCLUDED__
#include <string>
#include <vector>
#include <unordered_map>
#include <iostream>


class Parametres
{
    public:
        Parametres(std::string config);
    
        bool Load(std::string File);        
        
        int getProprieteI(std::string p);
        double getProprieteD(std::string p);
        std::string getProprieteS(std::string p);

        
        void setProprieteI(std::string p, int k);
        void setProprieteD(std::string p, double k);
        void setProprieteS(std::string p, std::string k);
        

        void initialise_default();
        void lit_parametres_fichier();
        void ecrit_parametres_fichier();

    private:
        static std::string Trim(const std::string& str);
        void remplir(std::string key, std::string value);
    
        std::unordered_map<std::string, double > m_proprietesD;
        std::unordered_map<std::string, int > m_proprietesI;
        std::unordered_map<std::string, std::string > m_proprietesS;

};



#endif
