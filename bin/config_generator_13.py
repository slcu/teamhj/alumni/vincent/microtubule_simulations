#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys

s="""Kgr_sh_Plus=0.01# /s                              allard et al 2010
Ksh_gr_Plus=0.04# /s                              allard et al 2010
V_gr_Plus=0.08# um/s
V_sh_Plus=0.16# um/s                              Kawamura et al 2008
V_sh_Moins=0.09# um/s                             Kawamura et al 2008
K_nobundle_shrink=0.4# um/s
K_nobundle_cross=0.6#1-m_proprietesD["K_nobundle_shrink
K_nobundle_catastrophe=0.4#                       Dixit et al 2004
poids_previous_vect=0.5
"""

dico={}
dico["nom_config"]="config.ini"
dico["Angle_bundle"]=0.69813170079773179
dico["D_bundle"]=49
dico["Angle_mb_limite"]=0.69813170079773179
dico["Angle_mb_trajectoire"]=0.69813170079773179
dico["Angle_cut"]=0.6;
dico["tan_Angle_mb"]=0.83909963117727993
dico["d_mb"]=10
dico["gr_st_plus"]=1
dico["gr_st_moins"]=0
dico["part_alea_alea"]=1
dico["part_alea_fixe"]=39
dico["epaisseur_corticale"]=1.
dico["cortical"]=0
dico["nb_microtubules_init"]=10    
dico["nb_max_steps"]=1000   
dico["garbage_steps"]=500
dico["vtk_steps"]=1000    
dico["part_influence_influence"]=0
dico["part_influence_normale"]=1
dico["nom_rapport"]=1
dico["nom_output_vtk"]="sortie_standart.vtk"
dico["nom_folder_output_vtk"]="./"
dico["nom_folder_input_vtk"]="./"
dico["nom_input_vtk"]="contourPave.vtk"
dico["nom_input_vtk_ref"]="contourPave_trou_NO.vtk"
dico["part_influence_influence"]=0
dico["part_influence_normale"]=1
dico["proba_detachement_par_step_par_microtubule"]=0.001
dico["proba_initialisation_par_step"]=0.2
dico["proba_tocut"]=0.01;
dico["taille_microtubule"]=8.
dico["decision_rencontre"]=0
dico["decision_accrochage"]=0
dico["decision_cut"]=0
dico["stop"]=0
dico["details"]=1#parametres destine à gere si on garde le contour ou pas
dico["save_events"]=100


if len(sys.argv)==1:
    print "arguments possibles :\n","\n".join([ i.ljust(40)+ " " + str(dico[i]) for i in sorted(dico.keys())])

dico2={}
if len(sys.argv)!=1:
    args=sys.argv[1:]
    for i in args:
        args_s = i.split("=")
        if args_s[0] in dico.keys():
            dico2[args_s[0]]=args_s[1]
        else:
            print "argument", args_s[0]," incompris"

filename=""
if "nom_config" in dico2:
    filename=dico2["nom_config"]
else:
    filename=dico["nom_config"]

f=open(filename, "w")

   
if len(sys.argv)>1:
    print "vos arguments :\n"
    for i in sorted(dico.keys()):
        if i in dico2.keys():
            print i.ljust(40)+ " " + str(dico2[i]) +  "  (default = "+str(dico[i])+")"
            f.write(str(i))
            f.write("=")
            f.write(str(dico2[i]))
            f.write("#")
            f.write(str(dico[i]))
            f.write("\n")
        else:
            print i.ljust(40)+ " " + str(dico[i])
            f.write(str(i))
            f.write("=")
            f.write(str(dico[i]))
            f.write("\n")

f.close()









